import { BrowserModule } from '@angular/platform-browser';
import { Component, NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { FooterComponent } from './Layouts/footer/footer.component';
import { RouterModule, Routes } from '@angular/router';
import { NavBarComponent } from './Layouts/nav-bar/nav-bar.component';
import { DashboardComponent } from './dashboard/dashboard.component';

const rutas: Routes = [
  {
    path: 'footer',
    component: FooterComponent
  },
  {
    path: 'nav',
    component: NavBarComponent
  },

{
  path: '',
  component: DashboardComponent
}

]
@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    NavBarComponent,
  ],

  imports: [
    BrowserModule,
    RouterModule.forRoot(rutas),
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
